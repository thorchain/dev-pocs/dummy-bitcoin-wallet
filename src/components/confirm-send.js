import React, { useState } from 'react';
import { Input, Radio, Space, Button, Alert } from 'antd';
import { useAsyncCallback } from 'react-async-hook';

import { sendNormalTx } from '../asgardex-bitcoin';

const radioStyle = {
  display: 'block',
  height: '30px',
  lineHeight: '30px'
};

const ConfirmSend = ({ address, feeEstimates }) => {
  const [sendAmount, setSendAmount] = useState('');
  const [feeOption, setFeeOption] = useState('regular');
  const trySend = useAsyncCallback(() => sendNormalTx(address, sendAmount, feeEstimates[feeOption].feeRate));

  if (trySend.result) {
    return (
      <Alert
        type="success"
        message="Transaction Successful"
        description={
          <div>
            <a
              href={`https://blockstream.info/testnet/tx/${trySend.result}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              View transaction
            </a>
          </div>
        }
      />
    );
  }

  return (
    <Space direction="vertical" size="large">
      <div><b>Destination:</b> {address}</div>
      <div>
        <Space>
          <b>Amount:</b>
          <Input
            value={sendAmount}
            onChange={e => setSendAmount(e.target.value)}
            suffix="sats"
            disabled={trySend.loading}
          />
        </Space>
      </div>
      <div>
        <Space align="start">
          <b>Fees:</b>
          <Radio.Group
            onChange={(e => setFeeOption(e.target.value))}
            value={feeOption}
            disabled={trySend.loading}
          >
            <Radio style={radioStyle} value="fast">
              Fast - {feeEstimates['fast'].feeTotal} sats
            </Radio>
            <Radio style={radioStyle} value="regular">
              Regular - {feeEstimates['regular'].feeTotal} sats
            </Radio>
            <Radio style={radioStyle} value="slow">
              Slow - {feeEstimates['slow'].feeTotal} sats
            </Radio>
          </Radio.Group>
        </Space>
      </div>
      <div>
        <Button
          onClick={trySend.execute}
          disabled={trySend.loading}
        >
          Send
        </Button>
      </div>
      <div>
        {trySend.error && <Alert message={trySend.error.message} type="error" />}
      </div>
    </Space>
  );
};

export default ConfirmSend;