import React, { useState } from 'react';
import { Input, Space, Button, Alert } from 'antd';
import { useAsyncCallback } from 'react-async-hook';

import { getFees } from '../asgardex-bitcoin';
import ConfirmSend from './confirm-send'

const SendForm = () => {
  const [address, setAddress] = useState('');
  const fees = useAsyncCallback(() => getFees());

  console.log('fees', fees.result)

  return (
    <>
      {fees.result ?
        <ConfirmSend
          address={address}
          feeEstimates={fees.result}
        />
        :
        <Space direction="vertical" size="large">
          <Space>
            <b>Address:</b>
            <Input
              value={address}
              onChange={e => setAddress(e.target.value)}
              style={{ width: 380 }}
              disabled={fees.loading}
            />
            <Button
              onClick={fees.execute}
              disabled={fees.loading}
            >
              Next
            </Button>
          </Space>
          {fees.error && <Alert message={fees.error.message} type="error" />}
        </Space>
      }
    </>
  );
};

export default SendForm;