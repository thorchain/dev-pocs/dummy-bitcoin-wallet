import React, { useState } from 'react';
import { Col, Card, Button, Input, Space, Alert } from 'antd';

import { generatePhrase, loadPhrase } from '../asgardex-bitcoin';

const SeedPhrase = ({ setPhrase }) => {
  const [inputPhrase, setInputPhrase] = useState('');
  const [errorMsg, setErrorMsg] = useState('');

  const newPhrase = () => {
    generatePhrase();
    setPhrase(true);
  };

  const loadInputPhrase = () => {
    try {
      loadPhrase(inputPhrase);
      setPhrase(true);
      setErrorMsg('')
    } catch (error) {
      setErrorMsg('Invalid BIP39 phrase')
    }
  };

  return (
    <Col xs={24} sm={16}>
      <Card bordered={false}>
        <Space direction="vertical" size="large">
          <Space>
            <b>Create new seed phrase:</b>
            <Button onClick={() => newPhrase()}>Create new seed phrase</Button>
          </Space>
          <Space>
            <b>Load existing seed phrase:</b>
            <Input
              value={inputPhrase}
              onChange={(e) => setInputPhrase(e.target.value)}
              style={{ width: 380 }}
            />
            <Button onClick={() => loadInputPhrase()}>Load seed phrase</Button>
          </Space>
          {errorMsg && <Alert message={errorMsg} type="error" />}
        </Space>
      </Card>
    </Col>
  );
};

export default SeedPhrase;