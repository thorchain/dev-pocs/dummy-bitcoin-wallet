import { Client, Network } from '@thorchain/asgardex-bitcoin';

const electrsAPI = 'http://165.22.106.224';
// const testPhrase = 'voyage brass middle distance cruise zebra virus onion unknown payment power crime';

const btcClient = new Client(Network.TEST, electrsAPI);

export const generatePhrase = () => {
  const newPhrase = btcClient.generatePhrase();
  loadPhrase(newPhrase);
  return newPhrase;
};

export const loadPhrase = (phrase) => {
  try {
    btcClient.setPhrase(phrase);
  } catch (error) {
    throw error;
  }
};

export const getAddress = () => {
  try {
    return btcClient.getAddress();
  } catch (error) {
    throw error;
  }
};

export const purge = () => {
  return btcClient.purgeClient();
};

export const getBalance = async () => {
  try {
    // btcClient.getAddress()
    // await btcClient.scanUTXOs()
    const balance = await btcClient.getBalance();
    console.log('balance', balance);
    return balance;
  } catch (error) {
    throw error;
  }
};

export const getTransactions = async () => {
  try {
    const address = btcClient.getAddress();
    return await btcClient.getTransactions(address);
  } catch (error) {
    throw error;
  }
};

export const getFees = async (memo) => {
  try {
    return await btcClient.calcFees(memo);
  } catch (error) {
    throw error;
  }
};

export const sendNormalTx = async (address, amount, feeRate) => {
  console.log(address, amount, feeRate)
  try {
    return await btcClient.normalTx(address, Number(amount), Number(feeRate));
  } catch (error) {
    console.log(error)
    throw error;
  }
};