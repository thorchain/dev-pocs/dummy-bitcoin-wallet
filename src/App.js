import React, { useState } from 'react';
import { Row, Col, Card, Tabs } from 'antd';

import SeedPhrase from './components/seed-phrase';
import Controls from './components/controls';
import TransactionsTable from './components/transactions-table';
import SendForm from './components/send-form';

const App = () => {
  const [phraseIsSet, setPhrase] = useState(false);

  return (
    <div className="app" style={{ backgroundColor: '#ececec', padding: 24 }}>
      <div style={{ textAlign: 'center' }}>
        <h1>Bitcoin Dummy Wallet</h1>
        <p>Note: you will need to force CORS to use this app
          &nbsp;
          <a
            href='https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere/'
            target="_blank"
            rel="noopener noreferrer"
          >
            Firefox addon
          </a>
        </p>
      </div>
      <Row justify="center">
        {!phraseIsSet ?
          <SeedPhrase setPhrase={setPhrase} />
          :
          <Col xs={24} sm={16}>
            <Card bordered={false}>
              <Controls setPhrase={setPhrase} />
              <Tabs defaultActiveKey="txs" size="large">
                <Tabs.TabPane tab="Transactions" key="txs">
                  <TransactionsTable />
                </Tabs.TabPane>
                <Tabs.TabPane tab="Send" key="send">
                  <SendForm />
                </Tabs.TabPane>
              </Tabs>
            </Card>
          </Col>
        }
      </Row>
    </div>
  );
};

export default App;
