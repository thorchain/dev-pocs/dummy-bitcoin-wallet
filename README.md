This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Dummy-bitcoin-wallet

Demo app for testing `asgardex-bitcoin`.

## Installation

1. Clone `dummy-bitcoin-wallet` into dev directory
2. Run `yarn install` inside `dummy-bitcoin-wallet`
3. Run `yarn start` inside `dummy-bitcoin-wallet`. The app will now be accessible on [http://localhost:3000](http://localhost:3000)
4. You will need a force CORS extension like [Cors Everywhere for Firefox](https://addons.mozilla.org/en-US/firefox/addon/cors-everywhere)
